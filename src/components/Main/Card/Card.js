import React from 'react';

import './Card.css';

function Card(props) {
  return (
    <div className="card">
      <h4 className="card_title" onClick={props.click}>
        {props.title}:{' '}
      </h4>
      <div className="labels">{props.labels}</div>
      <div className="card_footer">
        <p>#{props.number}</p>
        <p>{props.date}</p>
        <img src={props.avatar} alt="avatar" className="card_avatar" />
      </div>
    </div>
  );
}

export default Card;
