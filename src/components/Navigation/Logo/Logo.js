import React from 'react';

import './Logo.css';
import wolf from '../../../assets/images/jack-wolf-skin.png';

function Logo(props) {
  return (
    <li className="logo">
      <img src={wolf} alt="logo" />
      <h4>GitWoo</h4>
    </li>
  );
}

export default Logo;
