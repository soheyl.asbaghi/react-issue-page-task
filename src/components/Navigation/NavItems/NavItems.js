import React from 'react';

import NavItem from '../NavItem/NavItem';
import Logo from '../Logo/Logo';

import './NavItems.css';

function NavItems() {
  return (
    <ul className="nav-items">
      <Logo />
      <NavItem link="/">Home</NavItem>
      <NavItem link="/">Menu</NavItem>
    </ul>
  );
}

export default NavItems;
