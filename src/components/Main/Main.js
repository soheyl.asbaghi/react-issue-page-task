import React from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

import Columns from './Columns/Columns';
import Card from './Card/Card';
import Wrapper from '../../hoc/Wrapper';
import Loader from '../UI/Loader/Loader';
import Modal from '../UI/Modal/Modal';
import Destro from '../Destro/Destro';

import './Main.css';

class Main extends React.Component {
  state = {
    list: [],
    loading: true,
    error: false,
    modalShow: false,
    opened: true,
  };

  componentDidMount() {
    axios
      .get('/issues', {
        headers: {
          Authorization: 'Bearer fcBiuNs7sWzHsNzjcfr3',
        },
      })
      .then((res) => {
        console.log(res);
        this.setState({ list: res.data, loading: false });
      })
      .catch((err) => {
        console.log(err);
        this.setState({ error: true });
      });
  }

  modalHandler = () => {
    this.setState({ modalShow: true });
  };

  modalCloseHandler = () => {
    this.setState({ modalShow: false });
  };

  render() {
    console.log(
      this.state.list
        .map((item) => item.state)
        .filter((item) => item === 'opened')
    );
    return (
      <Wrapper className="main">
        {this.state.loading || !this.state.list ? (
          <Loader />
        ) : (
          <div className="main_col" key={uuidv4()}>
            <Columns title="Opened">
              {this.state.list.map((item) => {
                return item.state === 'opened' ? (
                  <Card
                    key={item.id}
                    click={this.modalHandler}
                    title={item.title}
                    labels={item.labels.map((lab) => {
                      return <p>{[lab]}</p>;
                    })}
                    number={item.iid}
                    date={item.created_at.slice(0, 10)}
                    avatar={item.assignee.avatar_url}
                  />
                ) : null;
              })}
            </Columns>

            <Columns title="Closed">
              {this.state.list.map((item) => {
                return item.state === 'closed' ? (
                  <Card
                    key={item.id}
                    click={this.modalHandler}
                    title={item.title}
                    labels={item.labels.map((lab) => {
                      return <p>{[lab]}</p>;
                    })}
                    number={item.iid}
                    date={item.created_at.slice(0, 10)}
                    avatar={item.assignee.avatar_url}
                  />
                ) : null;
              })}
            </Columns>
          </div>
        )}
        <Modal show={this.state.modalShow} modalClose={this.modalCloseHandler}>
          {this.state.list.map((item) => (
            <div className="main">
              {this.state.loading || !this.state.list ? (
                <Loader />
              ) : (
                <Destro
                  key={item.id}
                  title={item.title}
                  destro={item.description}
                />
              )}
            </div>
          ))}
        </Modal>
      </Wrapper>
    );
  }
}

export default Main;
