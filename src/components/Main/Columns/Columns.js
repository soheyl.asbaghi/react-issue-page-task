import React from 'react';

import './Columns.css';

const Columns = (props) => {
  return (
    <div className="columns">
      <div className="content">
        <div className="col-header">
          <h4 className="col-title">{props.title}</h4>
          <button className="col-btn">...</button>
        </div>
        <div className="column" status={props.status}>
          {props.children}
        </div>
      </div>
    </div>
  );
};

export default Columns;
