import React from 'react';

import './Loader.css';

function Loader() {
  return (
    <div className="loader">
      <div className="loader_content">
        <div className="duo duo1">
          <div className="dot dot-a"></div>
          <div className="dot dot-b"></div>
        </div>
        <div className="duo duo2">
          <div className="dot dot-a"></div>
          <div className="dot dot-b"></div>
        </div>
      </div>
    </div>
  );
}

export default Loader;
