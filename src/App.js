import React from 'react';
import axios from 'axios';

import Navbar from './components/Navigation/Navbar';
import Main from './components/Main/Main';

import './App.css';

axios.defaults.baseURL = 'https://gitlab.com/api/v4/projects/8350614';

function App() {
  return (
    <div className="container">
      <Navbar />
      <Main />
    </div>
  );
}

export default App;
