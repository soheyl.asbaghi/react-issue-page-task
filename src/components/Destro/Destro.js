import React from 'react';

import Wrapper from '../../hoc/Wrapper';

import './Destro.css';

function Destro(props) {
  return (
    <Wrapper>
      <h3>{props.title}</h3>
    </Wrapper>
  );
}

export default Destro;
